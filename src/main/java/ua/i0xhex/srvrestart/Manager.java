package ua.i0xhex.srvrestart;

public abstract class Manager {
    protected ServerRestart plugin;
    
    public Manager(ServerRestart plugin) {
        this.plugin = plugin;
    }
    
    public abstract void onEnable();
    
    public abstract void onDisable();
    
    public abstract void onReload();
}
