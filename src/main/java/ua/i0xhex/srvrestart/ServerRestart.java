package ua.i0xhex.srvrestart;

import org.bukkit.Bukkit;
import org.bukkit.command.PluginCommand;
import org.bukkit.plugin.java.JavaPlugin;

import ua.i0xhex.srvrestart.command.Cmd;
import ua.i0xhex.srvrestart.command.CmdServerRestart;
import ua.i0xhex.srvrestart.config.Config;
import ua.i0xhex.srvrestart.config.Lang;
import ua.i0xhex.srvrestart.manager.RestartManager;

public class ServerRestart extends JavaPlugin {
    
    private Lang lang;
    private Config config;
    private RestartManager restartManager;
    
    @Override
    public void onEnable() {
        lang = new Lang(this);
        config = new Config(this);
        registerCommand("serverrestart", new CmdServerRestart(this));
        restartManager = new RestartManager(this);
        restartManager.onEnable();
    }
    
    @Override
    public void onDisable() {
        restartManager.onDisable();
    }
    
    public void onReload() {
        lang = new Lang(this);
        config = new Config(this);
        restartManager.onReload();
    }
    
    // getters
    
    public Lang lang() {
        return lang;
    }
    
    public Config config() {
        return config;
    }
    
    public RestartManager getRestartManager() {
        return restartManager;
    }
    
    // internal
    
    private void registerCommand(String commandName, Cmd cmd) {
        PluginCommand command = getCommand(commandName);
        if (command != null) {
            command.setExecutor(cmd);
            command.setTabCompleter(cmd);
        } else {
            Bukkit.getLogger().warning(String.format("" +
                    "[ServerRestart] Command `%s` not found!", commandName));
        }
    }
}
