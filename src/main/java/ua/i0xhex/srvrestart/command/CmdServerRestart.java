package ua.i0xhex.srvrestart.command;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import ua.i0xhex.srvrestart.Permission;
import ua.i0xhex.srvrestart.ServerRestart;
import ua.i0xhex.srvrestart.config.Lang;

public class CmdServerRestart extends Cmd {
    private static final DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");
    
    public CmdServerRestart(ServerRestart plugin) {
        super(plugin);
    }
    
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (args.length > 0) {
            switch (args[0].toLowerCase()) {
                case "check":
                    exCheck(sender, args);
                    return true;
                case "reload":
                    exReload(sender, args);
                    return true;
                case "skip":
                    exSkip(sender, args);
                    return true;
                case "frestart":
                    exForceRestart(sender, args);
                    return true;
            }
        }
        
        plugin.lang().sendMessage(sender, "command.srt.help");
        return true;
    }
    
    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String s, String[] args) {
        if (args.length == 1) {
            return Arrays.stream(new String[] {"check", "reload", "frestart"})
                    .filter(a -> a.startsWith(args[0].toLowerCase()))
                    .collect(Collectors.toList());
        }
        return Collections.emptyList();
    }
    
    private void exCheck(CommandSender sender, String[] args) {
        Lang lang = plugin.lang();
        
        if (!sender.hasPermission(Permission.COMMAND_SRT_CHECK_TIME)) {
            lang.sendMessage(sender, "command._def.no-permission");
            return;
        }
        
        lang.sendMessage(sender, "command.srt.check.info",
                "{time}", getNextFormattedTime());
    }
    
    private void exReload(CommandSender sender, String[] args) {
        Lang lang = plugin.lang();
        
        if (!sender.hasPermission(Permission.COMMAND_SRT_RELOAD)) {
            lang.sendMessage(sender, "command._def.no-permission");
            return;
        }
        
        plugin.onReload();
        lang.sendMessage(sender, "command.srt.reload.info");
    }
    
    private void exSkip(CommandSender sender, String[] args) {
        Lang lang = plugin.lang();
    
        if (!sender.hasPermission(Permission.COMMAND_SRT_SKIP)) {
            lang.sendMessage(sender, "command._def.no-permission");
            return;
        }
        
        plugin.getRestartManager().scheduleSkipped();
        lang.sendMessage(sender, "command.srt.skip.info",
                "{time}", getNextFormattedTime());
    }
    
    private void exForceRestart(CommandSender sender, String[] args) {
        Lang lang = plugin.lang();
        
        if (!sender.hasPermission(Permission.COMMAND_SRT_FORCE_RESTART)) {
            lang.sendMessage(sender, "command._def.no-permission");
            return;
        }
        
        int seconds = 0;
        if (args.length > 1) {
            try {
                seconds = Integer.parseInt(args[1]);
                if (seconds < 0) throw new NumberFormatException();
            } catch (NumberFormatException ex) {
                lang.sendMessage(sender, "command.srt.frestart.invalid-seconds");
                return;
            }
        }
        
        plugin.getRestartManager().stopSchedule();
        plugin.getRestartManager().schedule(seconds * 1000);
        lang.sendMessage(sender, "command.srt.frestart.info",
                "{time}", getNextFormattedTime());
    }
    
    // internal
    
    private String getNextFormattedTime() {
        LocalDateTime time = plugin.getRestartManager().getNextRestartTime();
        return time == null ? "---" : TIME_FORMATTER.format(time);
    }
}
