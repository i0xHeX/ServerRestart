package ua.i0xhex.srvrestart;

public class Permission {
    public static final String COMMAND_SRT_CHECK_TIME = "srt.check";
    public static final String COMMAND_SRT_RELOAD = "srt.reload";
    public static final String COMMAND_SRT_SKIP = "srt.skip";
    public static final String COMMAND_SRT_FORCE_RESTART = "srt.frestart";
}
