package ua.i0xhex.srvrestart.action.actions;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;

import ua.i0xhex.srvrestart.action.Action;

public class ActionCommand extends Action {
    private static CommandSender sender = Bukkit.getConsoleSender();
    private String command;
    
    public ActionCommand(String data) {
        super(data);
        this.command = data;
    }
    
    @Override
    public void execute() {
        Bukkit.dispatchCommand(sender, command);
    }
}
