package ua.i0xhex.srvrestart.action.actions;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import ua.i0xhex.srvrestart.action.Action;

public class ActionSound extends Action {
    private Sound sound;
    
    public ActionSound(String data) {
        super(data);
        try {
            this.sound = Sound.valueOf(data.toUpperCase());
        } catch (Exception ex) {
            Bukkit.getLogger().warning(String.format("" +
                    "[ServerRestart] Sound not found: `%s`.", data));
            valid = false;
        }
    }
    
    @Override
    public void execute() {
        if (!valid) return;
        for (Player player : Bukkit.getOnlinePlayers())
            player.playSound(player.getLocation(), sound, 1.0F, 1.0F);
    }
}
