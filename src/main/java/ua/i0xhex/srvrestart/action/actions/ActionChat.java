package ua.i0xhex.srvrestart.action.actions;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import ua.i0xhex.srvrestart.action.Action;

public class ActionChat extends Action {
    private String message;
    
    public ActionChat(String data) {
        super(data);
        this.message = data;
    }
    
    @Override
    public void execute() {
        for (Player player : Bukkit.getOnlinePlayers())
            player.sendMessage(message);
    }
}
